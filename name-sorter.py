names = []

# open txt file and read the content in a list
with open('unsorted-names-list.txt', 'r') as openfile:  
    names = [current_name.rstrip() for current_name in openfile.readlines()]

# sort the name based on the last name
    sorted_names = sorted(names, key=lambda x: x.split(' ')[-1])
    print(*sorted_names, sep='\n')

# write the list into txt file
with open('sorted-names-list.txt', 'w') as writefile:  
    writefile.writelines("%s\n" % name for name in sorted_names)
    